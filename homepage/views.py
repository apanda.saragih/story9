from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def loginpage(request):
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			return redirect('homepage:menupage')
	else:
		form = AuthenticationForm()
	return render(request, 'login.html', {'form':form})

def landingpage(request):
    if request.user.is_authenticated:
        return redirect('homepage:logoutpage')
    else:
        return render(request, 'landing.html')

def menupage(request):
	if request.user.is_authenticated:
		return render(request,'menu.html')
	else:
		return redirect('homepage:loginpage')

def logoutpage(request):
	if request.user.is_authenticated:
		if request.method == 'POST':
			logout(request)
	return redirect('homepage:landingpage')